% Global contrast 

%% ------------- BEGIN CODE --------------
clear all
close all

%% Calculate global contrast for different speckle-to-pixel size ratios
load('sp2pixSize');

expTime=5; %mus 
avImages=zeros(length(speckleEst),length(speckleEst),length(estSpSize));
gCon = zeros(length(estSpSize),1);
for idx = 1:1:length(estSpSize)
    spSize=reqSpSize(idx);
    fileToLoad=(sprintf('speckleBrow_c0007_spS%.0f',spSize)); %=(sprintf('speckleLar_spS%.0f',spSize));
    load(fileToLoad); 
    
%     avImage5ms = mean(speckle(:,:,1:(expTime*1000)),3);
%     avImages(:,:,idx) = avImage5ms(:,:);
%     save(fileToLoad,'avImage5ms','expTime','-append');
    
    globalContrast=std(avImage5ms(:))./mean(avImage5ms(:));
    gCon(idx)=globalContrast;
    save(fileToLoad,'globalContrast','-append');
end     


%%

figure(4)
plot(reqSpSize,gCon)

