% Estimate XXX 
%% ------------- FROM MEETING WITH DMITRY -------------- %%
% ------------- USE KEENIES DATA FRA DROPBOX -------------- %
Close all
Clear all 
clc

%% Initialize
% Load data for speckle to pixel size ratio of 1
load('spRatio1') % Ratios of 1, 2, 3, 4, 6, 7, 8, 10

contrast = std(speckleBrowP(:,:,1))./mean(speckleBrowP(:,:,1),3);

%% Brownian motion

imgBrow5ms = mean(speckleBrowP(:,:,1:5000),3); % 5 ms of data
kernelSize = 3:2:51;
k = zeros(1,length(kernelSize));
for idx = 1:1:length(kernelSize)
k(idx) = mean(getSLSCI(imgBrow5ms,kernelSize(idx),'cpu','none'),[1,2]);
end

figure()
plot(kernelSize,k)
axis tight
title('Brownian Motion')
ylabel('Constrast')
xlabel('Kernel Size')

%% Ordered L

imgOrdL5ms = mean(speckleOrdL(:,:,1:5000),3); % 5 ms of data
kernelSize = 3:2:51;
k = zeros(1,length(kernelSize));
for idx = 1:1:length(kernelSize)
k(idx) = mean(getSLSCI(imgOrdL5ms,kernelSize(idx),'cpu','none'),[1,2]);
end

figure()
plot(kernelSize,k)
axis tight
title('Ordered Motion, Large Vessel')
ylabel('Constrast')
xlabel('Kernel Size')

%% Ordered M

imgOrdM5ms = mean(speckleOrdM(:,:,1:5000),3); % 5 ms of data
kernelSize = 3:2:51;
k = zeros(1,length(kernelSize));
for idx = 1:1:length(kernelSize)
k(idx) = mean(getSLSCI(imgOrdM5ms,kernelSize(idx),'cpu','none'),[1,2]);
end

figure()
plot(kernelSize,k)
axis tight
title('Ordered Motion, Medium Vessel')
ylabel('Constrast')
xlabel('Kernel Size')

%% Simulating dead space

figure()
title('Whole image')
imagesc(squeeze(mean(speckleOrdM(:,:,:),3)))
title('Whole image')

figure()
imagesc(squeeze(mean(speckleOrdM(:,:,1:2:end),3)))
title('Every second frame')

% faster flow = better estimation, so dead time does not affect the estimation much
% calculate contrast


%% Contrast vs. number of frames removed

I = speckleOrdM(:,:,:);
framesToRemove = 2:1:99;
K = zeros(1,length(framesToRemove));
for i=1:1:length(framesToRemove)
    idxs=1:1:size(I,3);
    idxs(mod(idxs,framesToRemove(i))==0)=[];
    img = round(mean(I(:,:,idxs),3));
    K(i) = squeeze(mean(getSLSCI(img,7,'cpu','none'),[1,2]));
end

figure()
plot(framesToRemove,K)
xlabel('Every N frames to remove')
ylabel('Contrast')

%% 
% try to calculate contrast at different exposure times
% it partially sovles the accuracy problem