%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 

%% Simulate all them damn speckles %% 

% Time
T = 5000; % no. of frames
dt = 1; % time steps of 1 mu second per frame

% Volume 
sizeXYZ = 100;% size of the volume (\mu m) 

% Particles
particlesN = 1000; % no. of particles

% Sensor
pixelsN = 100; % no. of pixels
pixelSize = [7.4359 3.5916 1.7348 1.1334 0.8379 0.6629]; % pixel sizes for speckle-to-pixel size ratios
Ratios = [1 2 4 6 8 10]; % speckle-to-pixel size ratios

% Optimal motion parameters (c value):    
cBrownianP = 0.0062; % constant for brownian motion (1/161)
cOrderedM = 0.064; % constant for ordered motion in medium vessel
cOrderedL = 0.2295; % constant for ordered motion in large vessel

% Simulate speckle pattern for different pixel sizes 
tic
for i = 1:1:length(pixelSize)
    pixRatio = Ratios(i);
    pixSize =  pixelSize(i);
    
    speckleOrdM = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(i),cOrderedM);     
    speckleOrdL = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(i),cOrderedL);       
    speckleBrowP = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(i),cBrownianP);  

    save(strcat('spRatio',num2str(pixRatio)),'speckleOrdM','speckleOrdL','speckleBrowP','T','pixRatio','pixSize','cOrderedM','cOrderedL','cBrownianP');  
end
toc

