% Estimate pixel sizes, to get speckle to pixel ratios 

%% ------------- BEGIN CODE --------------

%-------- simulation parameters -------- 
% Time
T = 1; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 
% Volume 
sizeXYZ = 100;% mum 
% Particles
particlesN = 1000;
% Sensor
pixelsN = 100; % 100x100 pixels, in theory we would like even larger

% Motion parameters 
% cBrownianP = 0.0062 (1/161), cOrderedM = 0.064, cOrderedL = 0.2295

% Now we are loading from the previous results!! cDisp is therefore
% slightly different
type = 'ordered'; % motion type for simulation
load tauFitOrd
cDisp = qp(2); % motion parameter for simulation

% Set different pixel sizes and evaluate the speckle to pixel size ratio. 
pixelSize = [1 2 4 6 8 10];

%% Simulate speckle patteren for different pixel sizes 

for i = 1:1:length(pixelSize)
    pix = pixelSize(i);
    speckle(:,:,i) = speckleSim(type,T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(i),cDisp);  
end

%% Visualize the speckle patterns 

figure('Name','Speckle Patterns')
montage(speckle)
axis image
title('Pixel Sizes')

%% Speckle size estimation
maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

sSize = NaN(length(pixelSize),1);
for i = 1:1:length(pixelSize)
    pix = pixelSize(i);
    [sSize(i),corr] = getSpeckleSize(speckle(:,:,i),maxLags); % compare to dmitry's corr
end 

%% Fitting the curve, to get the pixel sizes for ratios of [1 2 4 6 8 10]
[xData, yData] = prepareCurveData( pixelSize, sSize);
ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
% opts.StartPoint = [6.92047132512014 -0.952803010419301];

% Fit model to data.
[fitSp2Pix, ~] = fit( xData, yData, ft, opts );
% Goodness of fit: R-square: 0.9981
% General model Power1: f(x) = a*x^b,  a = 6.76, b = -0.9525

% Plot fit with data.
figure(1)
h = plot(fitSp2Pix,'--k', xData, yData,'ok');
legend( h, 'Speckle size vs. Pixel size', 'Power Fit', 'Location', 'NorthEast', 'Interpreter', 'none' );
xlabel( 'Pixel size', 'Interpreter', 'none' );
ylabel( 'Speckle size', 'Interpreter', 'none' );
grid on
axis tight
saveas(figure(1),'SpeckleSoverPixelS.png')

%% Save, this is all we need to save!

filename = strcat('sp2pixSize',type,num2str(round(cDisp,5)));
filename = strrep(filename,'.','');
save(filename,'speckle','pixelSize','sSize','fitSp2Pix');

%% Solving for ratio
reqRatio = [1 2 4 6 8 10]; % this is the ratio we want
estRatio = pixelSize'./fitSp2Pix(pixelSize); % this is the ratio we estimated
ft = fittype( 'power1' ); % model to fit
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
% Fit model to data.
[fitPix2SpRatio, ~] = fit( estRatio, pixelSize', ft, opts );

% Plot fit
figure(2)
plot(fitPix2SpRatio,'--k', estRatio, pixelSize,'ok'), hold on
estPixS = fitPix2SpRatio(reqRatio);
scatter(reqRatio,estPixS,'*k');
for i = 1:length(estPixS) 
    text(reqRatio(i),estPixS(i),sprintf("   [%.2f,%.2f]    ",...
        reqRatio(i),estPixS(i)),'Position',[1 0],'HorizontalAlignment','left')
end
legend('Data','Power Fit','Estimated Pixel Size','Location','northwest')
xlabel('Pixel to Speckle Ratio')
ylabel('Pixel Size')
axis tight
saveas(figure(2),'Pix2SpRatio.png')

%% Save ratio
filename = strcat('pix2spRatio',type,num2str(round(cDisp,5)));
filename = strrep(filename,'.','');
save(filename,'fitPix2SpRatio','estRatio','reqRatio','estPixS');

%% For reference, these are the old results!!
% Pixelsizes should be (in mum):
% 7.4359    3.5916    1.7348    1.1334    0.8379    0.6629
% to obtain the desired speckle-to-pixel ratios. 


%% ------------- END CODE --------------