clear all; close all; clc;

%% Add files to Matlab path

% Add data to path
addpath('Data/Speckles') % add path to Keenies new data files from dropbox
% Add functions to path
addpath('Functions') 

%% Create image of 5 ms

% --- New way of loading data, when the new data is uploaded. Now: error
% with sp ratio = 3 
% load('sp2pixSize'); % to get data of speckleSize ratios. 
% 
% gCon = zeros(length(estSpSize),1);
% for idx = 1:1:length(estSpSize)
%     spSize=reqSpSize(idx);
%     fileToLoad=(sprintf('speckleBrow_c0007_spS%.0f',spSize)); 
%     load(fileToLoad); 
% end
% ---

% Choose 1 file
files = ["speckleOrd_c0066_spS1","speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10"];
% files = ["speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
% files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10"];


% Choose motion type for file
type = "OrdM";
% type = "OrdL";
% type = "BrowP";

for i = 1:1:length(files)
    load(files(i));
    mImage(:,:,i) = mean(speckle(:,:,1:5000),3);
    
    % Saves averaged images according to motion type
    save(strcat('mData',type),'mImage');

end

%% Calculate Speckle Contrast vs. Kernel Size

% Choose 1 file to plot
files = "mDataOrdM";
% files = "mDataOrdL";
% files = "mDataBrowP";

load(files);

kernelSize = 1:2:49;
contrast = zeros(6,length(kernelSize));

%  for i = 1:1:length(files)
%     load(files(i));
%     

contrastImages = zeros(length(mImage), length(mImage), 6, length(kernelSize));

for k = 1:1:length(kernelSize)
    contrast(:, k) = mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);% mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);
    
    dimm = size(mImage);
    for j = 1:1:dimm(3)
        contrastImages(:,:,j,k) = getSLSCI(mImage(:,:,j),kernelSize(k),'cpu','none');     
    end
end

%% Remove border of images in relation to kernel size
contrastImagesCrop = contrastImages;

for k = 1:1:length(kernelSize)
    contrastImagesCrop(1:round(kernelSize(k)/2),:,:,k) = 0;
    contrastImagesCrop(end-round(kernelSize(k)/2):end,:,:,k) = 0;
    
        contrastImagesCrop(:,1:round(kernelSize(k)/2),:,k) = 0;
    contrastImagesCrop(:,end-round(kernelSize(k)/2):end,:,k) = 0;
end

% Visualize averaged images, kernelsize = 49
figure(1)
for i = 1:6 % length of files
    subplot(2,6,i)
    imagesc(contrastImages(:,:,i,end))
    axis('square')
    colormap gray
    minVal = min(min(min(min(contrastImages(:,:,:,:)))));
    maxVal = max(max(max(max(contrastImages(:,:,:,:)))));
    caxis([minVal maxVal]);
end

for i = 1:6 % length of files
    subplot(2,6,6+i)
    imagesc(contrastImagesCrop(:,:,i,end))
    axis('square')
    colormap gray
    caxis([minVal maxVal]);
end

%% Plot contrast vs. kernel size
% figure(2)
% plot(kernelSize,contrast)
% axis tight
% grid on
% legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
% ylabel('Contrast')
% xlabel('Kernel size')
% saves figure and a .png file in directory according to motion type
% saveas(figure(2),strcat('Contrast_vs_Kernel_size_',type,'.png'))

%% Find optimal trade off point
nPoints = length(kernelSize);
for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';

    % Pull out first point
    firstPoint = allCoord(1,:);

    % Create vector between first and last point
    lineVec = allCoord(end,:) - firstPoint;

    % Normalize vector
    lineVecN = lineVec / sqrt(sum(lineVec.^2));

    % Find the distance from each point to the line, by splitting data into 
    % perpendicular to the line and transversal.
    vecFromFirst = bsxfun(@minus, allCoord, firstPoint);

    scalarProduct = dot(vecFromFirst, repmat(lineVecN,nPoints,1), 2);
    vecFromFirstParallel = scalarProduct * lineVecN;
    vecToLine = vecFromFirst - vecFromFirstParallel;

    % Distance to line is the norm of vecToLine
    distToLine = sqrt(sum(vecToLine.^2,2));

    % Plot the distance to the line
    % figure('Name','distance from curve to line'), plot(distToLine)

    %Find the maximum aka the best trade off point
    [maxDist,idxOfBestPoint] = max(distToLine);

    idxOfBestPointAll(i) = idxOfBestPoint;
    idxOfBestPointAll(i) = idxOfBestPoint;
end

%% Plot contrast vs. kernel size with optimal kernel points
figure(2)
plot(kernelSize,contrast(:,:))
hold on

for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';
    plot(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), 'or','HandleVisibility','off') 
    textString = sprintf('  [%.2f, %.2f]', allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2));
    text(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), textString, 'FontSize', 10, 'Color', 'k');
end
legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
ylabel('Contrast')
xlabel('Kernel size')
axis tight
grid on

saveas(figure(2),strcat('Contrast_vs_Kernel_size_',type,'Optimal.png'))