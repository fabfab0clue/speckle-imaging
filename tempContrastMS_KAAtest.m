
% Calculate exposure time effects

% -----------------   Start here   -----------------   %

% choose files
files = ["speckleOrd_c0066_spS1", "speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10"];
% files = ["speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
% files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10"];

type = "OrdM";
% type = "OrdL";
% type = "BrowP";

% set exposure time
imageSize = size(matfile(files(1)), 'speckle');
splitSetting = [900,100]; % define frames for exposure time and dead time. Note: sum must be equal to numberOfFrames
splitPeriod = sum(splitSetting);
splitCount = idivide(int32(imageSize(3)), int32(splitPeriod));

% check if split equals frames
if mod(imageSize(3), splitPeriod) ~= 0
    error("ERROR: see split settings"); 
end

longSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % long splits are equal to exposure time
shortSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % short splits are equal to dead time

% generate images with exposure time
for i = 1:1:length(files)
    
    load(files(i));
    
    masterCursor = 1;
    for j = 1:splitCount
        % Cropping long segments
        masterCursorEnd = masterCursor + splitSetting(1) - 1;
        longSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;
        
        % Cropping short segments
        masterCursorEnd = masterCursor + splitSetting(2) - 1;
        shortSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;      
    end   
end

% Saves averaged images according to motion type
save(strcat('exposureData',type,num2str(splitSetting(1))),'longSplitsMean','shortSplitsMean','splitSetting');

% Visualize averaged images for set exposure times and varying speckle sizes
figure()
for i=1:size(longSplitsMean,4)    
    for j=1:size(longSplitsMean,3)
        subplot(size(longSplitsMean,3), size(longSplitsMean,4), i + (j-1) * size(longSplitsMean,4))
        imagesc(longSplitsMean(:,:,j,i));
        axis('square')
        colormap gray
    end
end

%% Comments: long- or shortSplitsMean = (X,Y,images,files) and SplitsMean(:,:,:,1) = 3-D matrix

files = "exposureDataOrdM900";
load(files);

% Get temporal contrast
kernel = 3; % temporal kernel size
tLSCI = zeros(6,1);

for i = 1:size(longSplitsMean,4)
    tLSCI(i) = mean(getTLSCI(longSplitsMean(:,:,:,i),kernel,'cpu','none'),[1,2,3]); % getTLSCI(longSplitsMean(:,:,j,i),kernel,'cpu','none'); % image size is changed to remove vignetting effect for contrast est.
end

sps = [1, 2, 4, 6, 8, 10];

figure(1)
plot(sps,tLSCI,'-ko')
xlabel('Speckle to pixel size ratios')
ylabel('Temporal contrast')










