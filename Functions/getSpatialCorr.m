function [corr,lags] = getSpatialCorr(I,maxLag)
%spatialAutoCorr spatial autocorrelation of speckle Image I

%   I = speckle image
%   maxLag = maximum number of lags

% OPTION 1
I_cent = I - mean(I(:)); 
I_transp = I_cent';
[corrX, lags] = xcorr(I_transp(:),maxLag,'unbiased');
corrY = xcorr(I_cent(:),maxLag,'unbiased');
corr = (corrX + corrY) ./ 2;
corr = corr./max(corr);

% OPTION 2
% corr2d = normxcorr2(I,I);
% idxC = [(size(corr2d,1)+1)/2, (size(corr2d,2)+1)/2]; % centrum [X Y]
% lags = -maxLag:1:maxLag; % vector of lag indices
% corrX = corr2d(idxC(1)+lags, idxC(2)); % corr moving in x direction
% corrY = corr2d(idxC(2), idxC(1)+lags); % corr moving in y direction
% corr = (corrX' + corrY) ./ 2;
% corr = corr./max(corr);
end

