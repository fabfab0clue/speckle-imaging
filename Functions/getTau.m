function [X,Y] = getTau(speckleFile1,speckleFile2,speckleFile3,fileType)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
% try something with nargin to find out wheter we have 3 or 4 input
% parameters

switch fileType
    case 'browP'
        load(speckleFile1);
        Y(1) = 1/tauCbrowP;
        X(1) = cBrownainP;
        load(speckleFile2);
        Y(2) = 1/tauCbrowP;
        X(2) = cBrownainP;
        load(speckleFile3);
        Y(3) = 1/tauCbrowP;
        X(3) = cBrownainP;
        
        % find the correct model, 2nd order maybe?
        
    case 'ordM'
        load(speckleFile1);
        Y(1) = 1/tauCordM;
        X(1) = cOrderedM(2);
        load(speckleFile2);
        Y(2) = 1/tauCordM;
        X(2) = cOrderedM(1);
        load(speckleFile3);
        Y(3) = 1/tauCordM;
        X(3) = cOrderedM(3);
        
%         beta = [ones(3,1) X]\Y;
%         x = 0:0.01:1;
%         y = [ones(length(x),1) x']*beta;
        
    case 'ordL'
        load(speckleFile1);
        Y(1) = 1/tauCordL;
        X(1) = cOrderedL(2);
        load(speckleFile2);
        Y(2) = 1/tauCordL;
        X(2) = cOrderedL(1);
        load(speckleFile3);
        Y(3) = 1/tauCordL;
        X(3) = cOrderedL(3);
        
%         beta = [ones(3,1) X]\Y;
%         x = 0:0.01:1;
%         y = [ones(length(x),1) x']*beta;
%         plot(x,y)

end

end

