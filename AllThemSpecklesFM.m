%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 

%% Simulate all them damn speckles %% 

% Time
T = 5000; % no. of frames
dt = 1; % time steps of 1 mu second per frame

% Volume 
sizeXYZ = 100;% size of the volume (\mu m) 

% Particles
particlesN = 1000; % no. of particles

% Sensor
pixelsN = 100; % no. of pixels

% Load estimated pixel and speckle size
load('sp2pixSize','estPixSize','estSpSize')

% Optimal motion parameters (c value):    
load('tauFitbrow','qp');
cBrownian = qp;
load('tauFitord','qp');
cOrdered = qp; % constant for ordered motion in [medium large] vessel


%% Simulate brownian motion
tic
for pix = 1:1:length(estPixSize)
    cDisp = cBrownian;
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    speckle = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixSize,cBrownian);  
    filename = strrep(sprintf('speckleBrow_c%.3f_spS%.0f',cDisp,spSize),'.','');
    save(filename,'speckle','T','cDisp');
end
toc

%% Simulate ordered motion 
tic
for pix = 1:1:length(estPixSize)
    for c = 1:1:length(cOrdered)
        cDisp = cOrdered(c);
        spSize = estSpSize(pix);
        pixSize = estPixSize(pix);
        speckle = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixSize,cOrdered(c));
        filename = strrep(sprintf('speckleOrd_c%.3f_spS%.0f',cDisp,spSize),'.','');
        save(filename,'speckle','T','cDisp','pixSize','spSize');
    end
end
toc

